public class QuestionUn extends MyThread {

    public QuestionUn(Peterson lock) {
        this.lock = lock;
    }

    public static void main(String[] args) {
        Peterson lock = new Peterson();
        QuestionUn q1 = new QuestionUn(lock);
        QuestionUn q2 = new QuestionUn(lock);
        q1.start();
        q2.start();
    }

}
