public class QuestionDeux extends MyThread {

    public QuestionDeux(Filter lock) {
        this.lock = lock;
    }

    public static void main(String[] args) {
        int n = 10;
        Filter lock = new Filter(n);
        QuestionDeux[] worker = new QuestionDeux[n];
        for(int i=0; i<n; ++i) {
            worker[i] = new QuestionDeux(lock);
        }
        for(int i=0; i<n; ++i) {
            worker[i].start();
        }
    }
}
