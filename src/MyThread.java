import java.util.concurrent.locks.Lock;

public class MyThread extends Thread {

    Lock lock;

    @Override
    public void run() {
        /*
            Section non critique
         */
        try {
            System.out.println("Section non critique");
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        lock.lock();

        System.out.println("Lock acquired by : " + ThreadID.get());

        /*
            Section critique
         */
        try {
            System.out.println("Section critique");
            Thread.sleep(1000);

        } catch (InterruptedException e) {

            e.printStackTrace();

        } finally {
            System.out.println("Unlocked by : " + ThreadID.get());
            lock.unlock();
        }
    }
}
