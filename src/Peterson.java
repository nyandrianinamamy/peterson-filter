import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/*
    This is a simple implementation of the Peterson algorithm to achieve mutual exclusion
    (only one thread in critic section at the same time) for 2 processes
 */

class Peterson implements Lock {
    private final boolean[] flag = new boolean[2];
    private volatile int victim;

    public Peterson() {
        flag[0] = false;
        flag[1] = false;
    }

    @Override
    public void lock() {
        int me = ThreadID.get();
        int other = 1 - me;

        flag[me] = true; // Indicates that I want to go to Critic Section
        victim = me; // Let the other go first (by courtesy)

        while (victim == me && flag[other]) { // Volatile field must precede flag to make flag visible by other threads
            // Busy-waiting while I am the victim and the other process want to go to critic section
        }
    }

    @Override
    public void unlock() {
        int me = ThreadID.get();
        flag[me] = false; // I went out of critic section
        victim = 1 - me;
    }

    @Override
    public void lockInterruptibly() throws InterruptedException {
    }

    @Override
    public boolean tryLock() {
        return false;
    }

    @Override
    public boolean tryLock(long l, TimeUnit timeUnit) throws InterruptedException {
        return false;
    }

    @Override
    public Condition newCondition() {
        return null;
    }
}
