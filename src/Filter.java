import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/*
    This is a simple implementation of the Filter algorithm, a general Peterson algorithm for n-processes

    Filter lock has n-1 levels which maybe considered as “waiting rooms”. A thread must traverse this waiting rooms before acquiring the lock.

    There are two important properties for levels:

    1) At least one thread trying to enter level l succeeds.
    2) If more than one thread is trying to enter level l, then at least one is blocked (i.e., continues to wait at that level).

 */
public class Filter implements Lock {

    /*
    We use AtomicInteger to guarantee linearizability and sequential consistency
     */
    private int IDLE = -1;
    private AtomicInteger[] victim;
    private AtomicInteger[] level;

    private int size;

    public Filter(int n) {
        this.size = n;
        level = new AtomicInteger[n];
        victim = new AtomicInteger[n];
        for (int i = 0; i < n; ++i) {
            level[i] = new AtomicInteger(IDLE);
            victim[i] = new AtomicInteger();
        }
    }

    @Override
    public void lock() {
        int me = ThreadID.get();

        for (int i = 0; i < size - 1; ++i) {
            level[me].set(i);
            victim[i].set(me);

            while (sameOrHigher(me, i) && victim[i].get() == me) {
            }
        }
        level[me].set(size - 1);

    }

    // Is there another thread at the same or higher level?
    private boolean sameOrHigher(int me, int myLevel) {
        for (int id = 0; id < size; id++)
            if (id != me && level[id].get() >= myLevel) {
                return true;
            }
        return false;
    }

    @Override
    public void unlock() {
        int me = ThreadID.get();
        level[me].set(IDLE);
    }

    @Override
    public void lockInterruptibly() throws InterruptedException {

    }

    @Override
    public boolean tryLock() {
        return false;
    }

    @Override
    public boolean tryLock(long l, TimeUnit timeUnit) throws InterruptedException {
        return false;
    }

    @Override
    public Condition newCondition() {
        return null;
    }
}
